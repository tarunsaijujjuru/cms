package com.softwareengineering.cms.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.softwareengineering.cms.exception.*;
import com.softwareengineering.cms.model.Student;
import com.softwareengineering.cms.model.Ticket;
import com.softwareengineering.cms.repository.TicketRepository;
import com.softwareengineering.cms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/users")
public class UsersController {
    @Autowired
    UserService userService;

    @Autowired
    TicketRepository ticketRepository;

    @PostMapping("/signup")
    public ResponseEntity<JsonNode> createUser(@RequestBody Student student){
        userService.createUser(student);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.createObjectNode();
        ((ObjectNode) rootNode).put("message", "created new student");
        return new ResponseEntity<>(rootNode, HttpStatus.OK);
    }

    @PostMapping("/validateUser")
    @ExceptionHandler({UserNotFoundException.class, InvalidCredentialsException.class})
    public ResponseEntity<JsonNode> validateUser(@RequestBody JsonNode utaIdPassword){
        userService.validateUser(utaIdPassword.get("utaId").asText(), utaIdPassword.get("password").asText());
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.createObjectNode();
        ((ObjectNode) rootNode).put("message", "User credentials are valid");
        return new ResponseEntity<>(rootNode, HttpStatus.OK);
    }

    @PostMapping("{utaId}/department/{departmentName}/createTicket")
    @ExceptionHandler({DepartmentDoesntExistsException.class})
    public ResponseEntity<JsonNode> createTicket(@RequestBody JsonNode description, @PathVariable String utaId, @PathVariable String departmentName){
        Ticket ticket = new Ticket();
        ticket.setStatus(0);
        ticket.setDescription(description.get("description").asText());
        userService.createTicket(utaId, departmentName, ticket);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.createObjectNode();
        ((ObjectNode) rootNode).put("message", "Ticket is created");
        return new ResponseEntity<>(rootNode, HttpStatus.OK);
    }

    @GetMapping("/{utaId}/viewTickets")
    public ResponseEntity<Set<Ticket>> viewTickets(@PathVariable String utaId){
        return new ResponseEntity<>(userService.viewTickets(utaId), HttpStatus.OK);
    }

    @PostMapping("updateTicket/{ticketId}/{status}")
    public ResponseEntity<JsonNode> updateTicket(@PathVariable String ticketId, @PathVariable String status){
        userService.updateTicket(ticketId, status);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.createObjectNode();
        ((ObjectNode) rootNode).put("message", "Ticket status is updated");
        return new ResponseEntity<>(rootNode, HttpStatus.OK);
    }

}
