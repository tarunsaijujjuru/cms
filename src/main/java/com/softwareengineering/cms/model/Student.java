package com.softwareengineering.cms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "students")
@JsonIgnoreProperties("tickets")
public class Student extends User {
    @Column
    private String apartmentNumber;

    @Column
    private String apartmentName;

    @Column
    private String major;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Set<Ticket> tickets;
}
