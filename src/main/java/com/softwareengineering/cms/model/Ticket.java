package com.softwareengineering.cms.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tickets")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column
    private String description;

    @Column
    private Integer status;

    @JsonBackReference
    @EqualsAndHashCode.Exclude
    @ManyToOne
    private Department department;

    @JsonBackReference
    @EqualsAndHashCode.Exclude
    @ManyToOne
    private Student student;
}
