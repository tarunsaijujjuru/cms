package com.softwareengineering.cms.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Data
@ResponseStatus(value = HttpStatus.FORBIDDEN, reason="Student already exists")
public class StudentAlreadyExistsException extends RuntimeException{
    String message;

    public StudentAlreadyExistsException(String emailId){
        this.message = "Student already exists with this emailId or utaId";
    }
}
