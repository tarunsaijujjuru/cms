package com.softwareengineering.cms.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="Ticket Status not valid")
public class TicketStatusNotValidException extends RuntimeException{
    private String message;

    public TicketStatusNotValidException(String message){
        this.message = message;
    }
}
