package com.softwareengineering.cms.service;

import com.softwareengineering.cms.exception.*;
import com.softwareengineering.cms.model.*;
import com.softwareengineering.cms.repository.AdminRepository;
import com.softwareengineering.cms.repository.DepartmentRepository;
import com.softwareengineering.cms.repository.StudentRepository;
import com.softwareengineering.cms.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    StudentRepository studentRepository;

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    TicketRepository ticketRepository;

    @Override
    public Boolean validateUser(String utaId, String password){
        User student = studentRepository.findByUtaId(utaId);
        User admin = adminRepository.findByUtaId(utaId);

        if(student == null && admin == null){
            throw new UserNotFoundException("User doesn't exist with utaId: " + utaId);
        }
        else{
            if((student != null && !student.getPassword().equals(password)) || (admin != null && !admin.getPassword().equals(password))){
                throw new InvalidCredentialsException(utaId);
            }
            return true;
        }
    }

    @Override
    public void createUser(Student student){
        Student retrievedUserByEmailId = studentRepository.findByEmailId(student.getEmailId());
        Student retrievedUserByUtaId = studentRepository.findByUtaId(student.getUtaId());
        Admin retrievedAdminByEmailId = adminRepository.findByEmailId(student.getEmailId());
        Admin retrievedAdminByUtaId = adminRepository.findByUtaId(student.getUtaId());
        if(retrievedUserByEmailId == null && retrievedUserByUtaId == null && retrievedAdminByEmailId == null && retrievedAdminByUtaId == null) {
            studentRepository.save(student);
        }
        else if(retrievedUserByEmailId != null || retrievedUserByUtaId != null){
            throw new StudentAlreadyExistsException(student.getEmailId());
        }
        else {
            throw new AdminAlreadyExistsException(student.getEmailId(), student.getUtaId());
        }
    }

    @Override
    public void createTicket(String utaId, String departmentName, Ticket ticket){
        Department department = departmentRepository.findByName(departmentName);
        if(department == null){
            throw new DepartmentDoesntExistsException(departmentName);
        }
        ticket.setDepartment(department);
        Student retrievedUserByUtaId = studentRepository.findByUtaId(utaId);
        if(retrievedUserByUtaId == null){
            throw new UserNotFoundException("User doesn't exist with utaId: " + utaId);
        }
        ticket.setStudent(retrievedUserByUtaId);
        ticketRepository.save(ticket);
    }

    @Override
    public Set<Ticket> viewTickets(String utaId){
        Student retrievedUserByUtaId = studentRepository.findByUtaId(utaId);
        Admin retrievedAdminByUtaId = adminRepository.findByUtaId(utaId);
        if(retrievedUserByUtaId == null && retrievedAdminByUtaId == null){
            throw new UserNotFoundException("User doesn't exist with utaId: " + utaId);
        }
        else if(retrievedUserByUtaId != null){
            return retrievedUserByUtaId.getTickets();
        }
        else{
            return retrievedAdminByUtaId.getDepartment().getTickets();
        }
    }

    @Override
    public void updateTicket(String ticketId, String status){
        Ticket ticket = ticketRepository.getOne(Long.parseLong(ticketId));
        int statusId;
        if("Created".equals(status))
            statusId = 0;
        else if("In Progress".equals(status))
            statusId = 1;
        else if("Completed".equals(status))
            statusId = 2;
        else
            throw new TicketStatusNotValidException("Ticket status " + status + " is not valid");
        ticket.setStatus(statusId);
        ticketRepository.save(ticket);
    }

}
