package com.softwareengineering.cms;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.softwareengineering.cms.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({StudentAlreadyExistsException.class, AdminAlreadyExistsException.class, DepartmentDoesntExistsException.class, InvalidCredentialsException.class, UserNotFoundException.class, TicketStatusNotValidException.class})
    public ResponseEntity<JsonNode> handleExceptions(Exception e) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.createObjectNode();
        ((ObjectNode) rootNode).put("message", e.getMessage());
        return new ResponseEntity<>(rootNode, HttpStatus.OK);
    }
}
